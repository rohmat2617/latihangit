package Childs;
import Parent.Products;

public class Items extends Products {
	
	private int price;

	public Items() {
		super();
	}

	public Items(String name,int price) {
		super(name);
		this.price = price;
	}
	
	//Implementasi Method Abstract pada child supaya menjadi bentuk konkrit dan jelas
	
	@Override
	public void printinformation() {
		System.out.println("Product Name " + getName());
		System.out.println("Product Price " + getPrice());
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	
}
