package Main;
import Parent.Shape;
import childs.Triangle;
import childs.Circle;

public class AppMain {

	public static void main(String[] args) {
		
		Shape lingkaran = new Circle(20,"biru");
		Shape segitiga = new Triangle(10, 15, "Merah");
		
		System.out.println("Luas Lingkaran Berwarna " +lingkaran.getColor() +" adalah " + lingkaran.getArea());
		System.out.println("Luas Segitiga Berwarna " + segitiga.getColor()+" adalah "+segitiga.getArea());
	}

}
