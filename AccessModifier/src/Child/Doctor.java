package Child;

import Parent.Person;

public class Doctor extends Person{
	
	public String specialist;

	public Doctor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Doctor(String name, String address, String specialist) {
		super(name, address);
		this.specialist = specialist;
	}
	
	public void sugery() {
		System.out.println("I can Suqery Operation ");
	}
	
	public void greeting() {
		System.out.println("Hello My Name Is " + name + ".");
		System.out.println("I Come From " + address + ".");
		System.out.println("My Occupation Is A " + specialist+ " Doctor.");
	}
}
