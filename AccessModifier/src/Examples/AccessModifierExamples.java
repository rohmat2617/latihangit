package Examples;

public class AccessModifierExamples {
	
	/*//Access Modifier Default (no-modifier)
	String name;
	String address;
	//Contoh Method Menggunakan Default Modifer
	void sayhello(){
		
	}*/
	
	/*//Access Modifier public
	public String name;
	public String address;
	//Contoh Method Menggunakan Public Modifer
	public void sayHello() {
		
	}*/
	
	/*//Access modifier Protected
	protected String name;
	protected String address;
	//Contoh Method Menggunakan protected Modifer
	protected void sayHello() {
		
	}*/
	
	/*//Access modifier Private
	private String name;
	private String address;
	//Contoh Method Menggunakan Private Modifer
	private void sayHello() {
	
	}*/
	
}
