package Parent;

public class Person {
	public String name;
	public String address;
	
	public Person(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

	public Person() {
		super();
	}
	
	public void greeting() {
		System.out.println("Hello My Name Is " + name + ".");
		System.out.println("I Come From " + address + ".");
	}
}
