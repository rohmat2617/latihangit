package Child;

import Parent.Person;

public class Doctor extends Person{
	
	private String specialist;

	public Doctor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Doctor(String name, String address, String specialist) {
		super(name, address);
		this.specialist = specialist;
	}
	
	public void sugery() {
		System.out.println("I can Suqery Operation ");
	}
	
	public void greeting() {
		System.out.println("Hello My Name Is " + getName() + ".");
		System.out.println("I Come From " + getAddress() + ".");
		System.out.println("My Occupation Is A " + specialist + " Doctor.");
	}
	
	public String getSpecialist() {
		return this.specialist;
	}
	//Setter
	public void setSpecialist(String specialist) {
		this.specialist = specialist;
	}
}
