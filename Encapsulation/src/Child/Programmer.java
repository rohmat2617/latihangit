package Child;

import Parent.Person;

public class Programmer extends Person {
	private String technology;

	public Programmer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Programmer(String name, String address, String technology) {
		super(name, address);
		this.technology = technology;
	}

	public void hacking() {
		System.out.println("I can hacking a Website" );
	}
	
	public void coding() {
		System.out.println("I can a create application using : " + technology + ".");
	}
	
	public void greeting() {
		super.greeting();
		System.out.println("My Job Is A " + technology + " Programmer.");
	}
	
	public String getTechnology() {
		return this.technology;
	}
	//Setter
	public void setTechnology(String technology) {
		this.technology = technology;
	}
	
	
}
