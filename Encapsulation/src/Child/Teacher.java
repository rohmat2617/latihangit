package Child;

import Parent.Person;

public class Teacher  extends Person{
	
	private String  subject;

	public Teacher() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Teacher(String name, String address, String subject) {
		super(name, address);
		this.subject = subject;
	}
	
	public void teaching() {
		System.out.println("I can teach " + subject + " So anyone how want to learn can talk to me");
	}
	
	public void greeting() {
		System.out.println("Hello My Name Is " + getName() + ".");
		System.out.println("I Come From " + getAddress() + ".");
		System.out.println("My Job Is A " +subject+ " Teacher.");
	
	}
	
	//getter dan setter
	public String getSubject() {
		return this.subject;
	}
	//Setter
	public void setSubject(String subject) {
		this.subject = subject;
	}
}
