package Main;

import Child.Doctor;
import Child.Teacher;
import Child.Programmer;
import Parent.Person;

public class AppMain {

	public static void main(String[] args) {

		Person person1 = new Programmer("Rizky","Bandung",".Net core");
		Person person2 = new Teacher("Joko","Tegal","Matematika");
		Person person3 = new Doctor("Eko","Surabaya","Pedistrician");
		
	
		sayHello(person1);
		sayHello(person2);
		sayHello(person3);
		
		Person person4 = new Person ();
		person4.setName("Rizki");
		person4.setAddress("Bandung");
		
		System.out.println(person4.getName());
		System.out.println(person4.getAddress());
	}
	
		static void sayHello(Person person) {
			String message;
			if(person instanceof Programmer) {
				Programmer programmer = (Programmer)person;
				message = "Hello " + programmer.getName() + " Seorang Programmer " + programmer.getTechnology() +".";
			}else if(person instanceof Teacher) {
				Teacher teacher = (Teacher)person;
				message = "Hello " + teacher.getName() + " Seorang Guru " + teacher.getSubject() +".";
			}else if(person instanceof Doctor) {
				Doctor doctor = (Doctor)person;
				message = "Hello " + doctor.getName() + " Seorang Dokter " + doctor.getSpecialist() +".";
			}else {
				message = "Hello " + person.getName() + ".";
			}
			
			System.out.println(message);
		}
	


	}


