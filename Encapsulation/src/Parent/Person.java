package Parent;

public class Person {
	
	private String name;
	private String address;
	
	public Person(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

	public Person() {
		super();
	}
	
	public void greeting() {
		System.out.println("Hello My Name Is " + name + ".");
		System.out.println("I Come From " + address + ".");
	}
	
	// Getter dan Setter
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	// Getter
	public String getAddress() {
		return this.address;
	}
	//Setter
	public void setAddress(String address) {
		this.address = address;
	}
}
