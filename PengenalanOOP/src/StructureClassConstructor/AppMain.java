package StructureClassConstructor;


public class AppMain {

	public static void main(String[] args) {
		
		//Membuat Object dari class Person menggunakan Constructor parameter
		Person person1 = new Person("Eko","Tegal");
		person1.sayHello("Padepokan 79");
		System.out.println(person1.sayAddress());
		
		//Membuat Object dari class Person menggunakan Cosntructor default
		Person person2 = new Person();
		person2.name = "Joko";
		person2.address="Surabaya";
		person2.sayHello("Padepokan 79");
		System.out.println(person2.sayAddress());
		
		//Membuat Object dari class Person menggunakan Cosntructor dengan satu parameter
		Person person3 = new Person("Budi");
		person3.address="Bandung";
		person3.sayHello("Padepokan 79");
		System.out.println(person3.sayAddress());
	}

}
