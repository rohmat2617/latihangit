package StructureClassConstructor;

public class Person {
	
	String name;
	String address;
	
	final String country = "Indonesia";
	
	//Constructor Default
	Person(){
			
	}
		
	//Constructor Dengan Parameter
	Person(String paramName, String paramAddress){
			this(paramName);
			address = paramAddress;
	}
		
	//Cosntructor dengan satu parameter
	
	Person(String paramName){
			name = paramName; 
	}
	
	
		String sayAddress() {
			return "I, Come From " + address + " .";
		}
		void sayHello (String paramName) {
			System.out.println("Hello " + paramName+ ", My name Is " + name + ".");
		}
}
