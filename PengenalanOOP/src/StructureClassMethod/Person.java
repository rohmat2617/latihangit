package StructureClassMethod;

public class Person {
	
	String name;
	String address;
	
	final String country = "Indonesia";
	
	//Method Void
	void sayHello (String paramName) {
		System.out.println("Hello " + paramName+ ", My name Is " + name + ".");
	}
	
	//Method Return Value (Mengembalikan Nilai)
		String sayAddress() {
			return "I, Come From " + address + " .";
		}
}
