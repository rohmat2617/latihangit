package VariableShadowing;

public class MainApp {

	public static void main(String[] args) {
		
		
		//Membuat object dari class menggunakan constructor parameter
		Person person2 = new Person("Eko","Tegal");
		person2.sayHello("Padepokan 79");
		System.out.println(person2.sayAddress());
		
		//constructor default
		Person person3 = new Person();
		person3.name = "Joko";
		person3.address="Surabaya";
		person3.sayHello("Padepokan 79");
		System.out.println(person3.sayAddress());
		
		//Constructor dengan menggunakan satu parameter
		Person person4 = new Person("Budi");
		person4.address="Bandung";
		person4.sayHello("Padepokan 79");
		System.out.println(person4.sayAddress());
		
	}

}
