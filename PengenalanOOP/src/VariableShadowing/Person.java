package VariableShadowing;

public class Person {
	String name;
	String address;
	
	final String country = "Indonesia";
	
	//Constructor Default
	Person(){
		
	}
	//Construcor dengans satu parameter
	Person(String parnama){
		name = parnama;
	}
	//Constructor Dengan Parameter
	
	//shadowing 
	/*Person( String name, String address){
		//penamaan nama parameter dan field sama , sehingga terjadi shadowing variable
		name=name; //field nama tidak akan berubah namanya
		address = address;//field alamat tidak akan berubah namanya
	}*/
	//Solusi
	Person( String name, String address){
		this.name=name; //this.name ini menunjukan menunjukan field name adalah dari class Person itu sendiri
		this.address = address;//this.address ini menunjukan menunjukan field address adalah dari class Person itu sendiri
	}
	// Method Void
	void sayHello (String paramName) {
		System.out.println("Hello " + paramName+ ", My name Is " + name + ".");
	}
	//Method Return Value
	String sayAddress() {
		return "I, Come From " + address + " .";
	}
}
