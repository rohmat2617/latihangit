package Polymorphism;

public class Doctor extends Person{
	
	String specialist;

	public Doctor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Doctor(String name, String address, String specialist) {
		super(name, address);
		this.specialist = specialist;
	}
	
	void sugery() {
		System.out.println("I can Suqery Operation ");
	}
	
	void greeting() {
		System.out.println("Hello My Name Is " + name + ".");
		System.out.println("I Come From " + address + ".");
		System.out.println("My Occupation Is A " + specialist+ " Doctor.");
	}
}
