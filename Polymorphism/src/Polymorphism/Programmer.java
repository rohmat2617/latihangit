package Polymorphism;

public class Programmer extends Person {
	String technology;

	public Programmer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Programmer(String name, String address, String technology) {
		super(name, address);
		this.technology = technology;
	}

	void hacking() {
		System.out.println("I can hacking a Website" );
	}
	
	void coding() {
		System.out.println("I can a create application using : " + technology + ".");
	}
	
	void greeting() {
		super.greeting();
		System.out.println("My Job Is A " + technology + " Programmer.");
	}
	
	
}
