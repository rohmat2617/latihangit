package Polymorphism;

public class Teacher  extends Person{
	
	String  subject;

	public Teacher() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Teacher(String name, String address, String subject) {
		super(name, address);
		this.subject = subject;
	}
	
	void teaching() {
		System.out.println("I can teach " + subject + " So anyone how want to learn can talk to me");
	}
	
	void greeting() {
		System.out.println("Hello My Name Is " + name + ".");
		System.out.println("I Come From " + address + ".");
		System.out.println("My Job Is A " +subject+ " Teacher.");
	
	}
}
